Fieldable Payment Method

Overview
--------
The Fieldable Payment Method alters the Payment Method entity type and make it
fieldable.